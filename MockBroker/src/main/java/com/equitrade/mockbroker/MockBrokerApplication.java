package com.equitrade.mockbroker;

import java.util.Date;
import java.util.UUID;

import javax.jms.ConnectionFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@SpringBootApplication
@EnableJms
public class MockBrokerApplication {

    private static Logger logger = LoggerFactory.getLogger(MockBrokerApplication.class);
	private static final String source = "OrderBroker";
    
	public static void main(String[] args) {
		ConfigurableApplicationContext context = 
				SpringApplication.run(MockBrokerApplication.class, args);
		
		logger.info("BROKER IS RUNNING!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("BROKER IS RUNNINNGGGGGGGGG");
		
//		JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
//		
//		try {
//        	Thread.sleep(2000);
//        	
//        	jmsTemplate.convertAndSend(source, new BrokerOrders(0, true, false, 0, 88.0, 2000, 0, new Date(),
//        			OrderStatus.PENDING, "AAPL"),
//        			m -> {
//        		logger.info("Setting correlation id before sending message");
//        		m.setJMSCorrelationID(UUID.randomUUID().toString());
//        		return m;
//        	});
//        	
//    		System.out.println("Order sent.");
//            logger.info("Order sent.");
//        } 
//        catch (Exception ex) {}
		
	}
	
	@Bean
    public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

        // This provides all boot's default to this factory, including the message converter.
        // You can override some of Boot's default if necessary.
        configurer.configure(factory, connectionFactory);

        return factory;
    }
	
	// Serialize message content to JSON using TextMessage
	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_id");
		return converter;
	}
	
	

}
