package com.equitrade.mockbroker;

public enum OrderStatus {
	PENDING,PARTIALLY_FILLED,FILLED,REJECTED;
}
