package com.equitrade.mockbroker;

import java.util.Date;

public class BrokerOrders {
	
	private int orderId;
	private boolean buy;
	private boolean exits;
	private int retryCount;
	private double price;
	private int reqQuantity;
	private int fillQuantity;
	private Date orderDate;
	private OrderStatus status;
	
	private String stockCode;
	
	
	public BrokerOrders() {}
	
	public BrokerOrders(int orderId, boolean buy, boolean exits, int retryCount,  double price,
			int reqQuantity, int fillQuantity, Date orderDate, OrderStatus status, String stockCode) {
		this.orderId = orderId;
		this.buy = buy;
		this.exits = exits;
		this.retryCount = retryCount;
		this.price = price;
		this.setReqQuantity(reqQuantity);
		this.setFillQuantity(fillQuantity);
		this.setOrderDate(orderDate);
		this.setStatus(status);
		this.stockCode = stockCode;
	}
	
	public int getOrderId() {
		return orderId;
	}
	
	public void setId(int orderId) {
		this.orderId = orderId;
	}

	public boolean isBuy() {
		return buy;
	}
	
	public void setBuy(boolean buy) {
		this.buy = buy;
	}
	
	public boolean isExits() {
		return exits;
	}
	
	public void setExits(boolean exits) {
		this.exits = exits;
	}
	
	public int getRetryCount() {
		return retryCount;
	}
	
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getReqQuantity() {
		return reqQuantity;
	}

	public void setReqQuantity(int reqQuantity) {
		this.reqQuantity = reqQuantity;
	}
	
	public int getFillQuantity() {
		return fillQuantity;
	}

	public void setFillQuantity(int fillQuantity) {
		this.fillQuantity = fillQuantity;
	}
	
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public String getStockCode() {
		return stockCode;
	}
	
	public void setStock(String stockCode) {
		this.stockCode = stockCode;
	}
	
	@Override
	public String toString() {
		return String.format("Order{orderId=%s, buy=%s, exits=%s, retryCount=%s, price=%s,"
				+ "requiredQuantity=%s, fillQuantity=%s, orderDate=%s, status=%s , stock=%s",
				getOrderId(), isBuy(), isExits(), getRetryCount(), getPrice(), getReqQuantity(),
				getFillQuantity(), getOrderDate(), getStatus(), getStockCode());
	}

	

	

	

}
