import React, {Component} from 'react';

import { Colors } from "@blueprintjs/core";

import
{
  Alignment,
  Button,
  Classes,
  Tab,
  Tabs,
  H3,
  H4,
  H5,
  TabId,
  InputGroup,
  MenuItem,
  NumericInput,
  Alert,
  Toaster,
  Intent,
  Position

} from "@blueprintjs/core";

import AmountInput  from "./AmountInput"
//import * as Stocks from "./stocks";
import * as Strategies from "./strategies";
import { Select } from "@blueprintjs/select";
import axios from 'axios';

import './App.css';
const STOCKS: IStock[] = [];
const TOAST_MESSAGE = "";
const parseString = require('xml2js').parseString;

export interface IStock {
     CompanyName: string;
     Symbol: string;
     SymbolID: number;
}

export default class BuySellOrder extends React.Component{
  constructor() {
    super();
    this.state = {
      // stock: Stocks.STOCKS[0],
      stock: STOCKS[0],
      strategy: Strategies.STRATEGIES[0],
      STOCKS: [],
      value: 0,
      marketPrice: 0,
      wallet: 10000000,
      isOpen: false
    }
    this.getMinAmount = this.getMinAmount.bind(this);
    this.handleStockValueChange = this.handleStockValueChange.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.value!=nextState.value)
    {
      return false;
    }
    return true;
  }

  componentWillMount() {
    fetch('http://feed2.conygre.com/API/StockFeed/GetSymbolList')
    //return response;
         .then((response) => response.text())
         .then((responseText) => {
            var self = this;
             parseString(responseText, function (err, result){
                 // console.log(responseText);
                 // console.log(typeof(responseText));
                 var stockJson = responseText;
                 const stockObj = JSON.parse(stockJson);
                  // console.log("@@@@" + stockObj[0]);
                  const STOCKS: IStock[] = stockObj;
                  // console.log(this);
                  self.setState({ stock: STOCKS[0],
                  STOCKS: stockObj });
                  // console.log(this.state.STOCKS);
                  }.bind(this));

             })
         .catch((err) => {
             console.log('Error fetching the feed: ', err)
           });

  }

  renderStock: ItemRenderer<IStock> = (stock, { handleClick, modifiers, query }) => {
      if (!modifiers.matchesPredicate) {
          return null;
      }
      const text = `${stock.Symbol.toUpperCase()}`;
      return (
          <MenuItem
              active={modifiers.active}
              disabled={modifiers.disabled}
              label={stock.CompanyName}
              key={stock.SymbolID}
              onClick={handleClick}
              text={this.highlightText(text, query)}
              shouldDismissPopover={false}
          />
      );
  };

  filterStock: ItemPredicate<IStock> = (query, stock, _index, exactMatch) => {
    const normalizedTitle = stock.Symbol.toLowerCase();
    const normalizedQuery = query.toLowerCase();

    if (exactMatch) {
        return (normalizedTitle === normalizedQuery)
    }

    else {
        return `${stock.SymbolID}. ${normalizedTitle} ${stock.CompanyName}`.indexOf(normalizedQuery) >= 0;
    }
  };

 highlightText(text: string, query: string) {
    let lastIndex = 0;
    const words = query
        .split(/\s+/)
        .filter(word => word.length > 0)
        .map(this.escapeRegExpChars);
    if (words.length === 0) {
        return [text];
    }
    const regexp = new RegExp(words.join("|"), "gi");
    const tokens: React.ReactNode[] = [];
    while (true) {
        const match = regexp.exec(text);
        if (!match) {
            break;
        }
        const length = match[0].length;
        const before = text.slice(lastIndex, regexp.lastIndex - length);
        if (before.length > 0) {
            tokens.push(before);
        }
        lastIndex = regexp.lastIndex;
        tokens.push(<strong key={lastIndex}>{match[0]}</strong>);
    }
    const rest = text.slice(lastIndex);
    if (rest.length > 0) {
        tokens.push(rest);
    }
    return tokens;
  }

 escapeRegExpChars(text: string) {
    return text.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
  }


  // handleClick() {
  //  console.log("CLICKED");
  //
  // }

  getMinAmount(){
    console.log("MIN AMOUNT");
    console.log(this.state.stock.Symbol);
    const stockCode = this.state.stock.Symbol
    console.log("$$$ " + stockCode)
    axios.get("http://localhost:8081/CurrentMarketPrice/" + stockCode).then(res =>{
      console.log(res);
      console.log(res.data);
      this.setState({
        marketPrice: res.data
      })
      console.log(this.state.marketPrice);
    })
  }

  handleStockValueChange = (stock: IStock) => {
    this.setState(
      {stock
      }, this.getMinAmount)
    console.log(stock.Symbol + "%%%");
  }

  handleStrategyValueChange = (strategy: Strategies.IStrategy) => this.setState({ strategy })
  handleValueChange= (valueAsNumber: number, valueAsString: string) => {
    this.setState(state => ({value: valueAsNumber}));
  }

  handleMoveOpen = () => {
    this.setState({ isOpen: true });
    let strategyOrder = {
      requestStockCode: this.state.stock.Symbol.toUpperCase(),
      requestStrategyCode: this.state.strategy.title,
      requestAmnt: this.state.value,
    }
    console.log("POSTING...  " + strategyOrder.requestStockCode + ", "+ strategyOrder.requestStrategyCode + ", " + strategyOrder.requestAmnt + ": ");
    axios.post("http://localhost:8081/StrategyOrder", strategyOrder).then(res => {
      console.log(res);
      this.setState({
        messageFromServer: res.data
      });
    });
  }

  handleMoveConfirm = () => {
    this.setState({ isOpen: false });
    this.toaster.show({
      icon: "tick",
      intent: Intent.SUCCESS,
      position: Position.BOTTOM,
      message: "Stock order has been successfully submitted."
    });
  };
  handleMoveCancel = () => this.setState({ isOpen: false });

  render(){
    const BuySellPanel: React.FunctionComponent<{}> = () => (
    <div id = "buysell-order" align = "left" style={{paddingLeft: 90}} >
      <div id = "select-stock" style={{display: "inline-block", textAlign: "left", marginTop: 20}}>
        <Select
            items={this.state.STOCKS}
            itemPredicate={this.filterStock}
            itemRenderer={this.renderStock}
            onItemSelect={this.handleStockValueChange}
            noResults={<MenuItem disabled={true} text="No results." />}
            filterable={true}
            minimal={true}
        >
            {/* children become the popover target; render value here */}
             <text> Select stocks </text>
            <br/>
            {(this.state.stock == undefined) || (this.state.stock == 'undefined') ?(
              <Button text={""} rightIcon="caret-down" />
            ) : (
              <Button text={this.state.stock.Symbol.toUpperCase()} rightIcon="caret-down" />
            )}
        </Select>
      </div>
      <br/>
      <div id = "select-strategy" style={{display: "inline-block", textAlign: "left"}} >
        <br/>
        <Select
          items={Strategies.STRATEGIES}
          itemRenderer={Strategies.renderStrategy}
          noResults={<MenuItem disabled={true} text="No results." />}
          onItemSelect={this.handleStrategyValueChange}
          filterable={false}
          minimal={true}
        >
          <text> Select strategies </text>
          <br/>
          <Button text={this.state.strategy.title} rightIcon="caret-down" />
        </Select>
      </div>

      <div style={{display: "inline-block", textAlign: "left",marginTop: 20}}>
      <text>Amount</text>
        <NumericInput
          leftIcon="dollar"
          placeholder="10,000,000"
          max={this.state.wallet}
          min={this.state.marketPrice}
          onValueChange={this.handleValueChange}
          value={this.state.value}
          allowNumericCharactersOnly={true}
          selectAllOnFocus= {true}
          selectAllOnIncrement= {false}
          minorStepSize= {0.01}
          majorStepSize= {10}
          clampValueOnBlur={true}
        />
      </div>

      <br/>
      <Button text="REVIEW" onClick={this.handleMoveOpen} style={{width:220, textAlign:"center", display: "inline-block", marginTop: 50}} />

    </div>
    );

        return(
              <div style={{backgroundColor: "#202B33", width: 500, height: 400}}>
                <Tabs className={Classes.DARK}>
                  <div className="bp3-tab-list" role="tablist" style={{flex: 1}}> </div>
                    <Tab id="buysell" title="BUY/SELL" panel={<BuySellPanel/>} />
                    <Tabs.Expander />
                </Tabs>

                {(this.state.stock != undefined) || (this.state.stock == 'undefined') ?(
                  <Alert
                    className={Classes.DARK}
                    cancelButtonText="Cancel"
                    confirmButtonText="Submit"
                    icon="updated"
                    intent={Intent.SUCCESS}
                    isOpen={this.state.isOpen}
                    onCancel={this.handleMoveCancel}
                    onConfirm={this.handleMoveConfirm}
                    canOutsideClickCancel = {true}
                  >
                    <p>
                        <br/>
                        <H4 style={{color: "#3DCC91"}}>You have selected:</H4><br/>
                        <H3>{this.state.stock.Symbol.toUpperCase()} | ${this.state.marketPrice} <text style={{fontSize: 10}}>per stock</text></H3>
                        {this.state.stock.CompanyName}<br/>
                        <br/>
                        <text style={{color: "#3DCC91"}}> Strategy: </text>
                        <H5>{this.state.strategy.title}</H5>

                        <text style={{color: "#3DCC91"}}>Amount: </text><br/>
                        <H5>${this.state.value}</H5>


                    </p>
                  </Alert>
                ) : (
                  null
                )}

                <Toaster ref={ref => (this.toaster = ref)} />
              </div>


      )}
}
