import React, {component} from 'react';
import {Cell, Column, Table, ColumnHeaderCell,EditableName, EditableCell, Regions } from "@blueprintjs/table";
import {ItemRenderer, MultiSelect, Select} from "@blueprintjs/select";
import BuySellOrder from "./BuySellOrder";
import axios from 'axios';
import * as Strategies from "./strategies";
import
{
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames, 
  button, 
  Colors, 
  MenuItem

} from "@blueprintjs/core";

import './App.css';

import { ALIGN_RIGHT } from '@blueprintjs/icons/lib/esm/generated/iconContents';

const STOCKS: IStock[] = [];
const TOAST_MESSAGE = "";
const parseString = require('xml2js').parseString;
const INTENTS = [Intent.NONE, Intent.PRIMARY, Intent.SUCCESS, Intent.DANGER, Intent.WARNING]
const StockMultiSelect = MultiSelect.ofType();
export interface IStock {
     CompanyName: string;
     Symbol: string;
     SymbolID: number;
}

export default class MarketDataTable extends React.Component{

  constructor() {
    super();
    this.state = {
      // stock: Stocks.STOCKS[0],
      stock: STOCKS[0],
      strategy: Strategies.STRATEGIES[0],
      STOCKS: [],
      value: 0,
      marketPrice: 0,
      wallet: 10000000,
      isOpen: false,
      selectedStocks: [],
      selectStock: null
    }
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.value!=nextState.value)
    {
      return false;
    }
    return true;
  }
  

  componentWillMount() {
    fetch('http://feed2.conygre.com/API/StockFeed/GetSymbolList')
    //return response;
         .then((response) => response.text())
         .then((responseText) => {
            var self = this;
             parseString(responseText, function (err, result){
                 // console.log(responseText);
                 // console.log(typeof(responseText));
                 var stockJson = responseText;
                 const stockObj = JSON.parse(stockJson);
                  // console.log("@@@@" + stockObj[0]);
                  const STOCKS: IStock[] = stockObj;
                  // console.log(this);
                  self.setState({ stock: STOCKS[0],
                  STOCKS: stockObj });
                  // console.log(this.state.STOCKS);
                  }.bind(this));

             })
         .catch((err) => {
             console.log('Error fetching the feed: ', err)
           });

  }
  renderStock: ItemRenderer<IStock> = (stockItem, { handleClick, modifiers}) => {
    console.log("RENDER STOCK");
      if (!modifiers.matchesPredicate) {
          return null;
      }
      return (
          <MenuItem
              active={modifiers.active}
              disabled={modifiers.disabled}
              label={stockItem.CompanyName}
              key={stockItem.SymbolID}
              onClick={this.handleClick}
              text={`${stockItem.Symbol}`}
              shouldDismissPopover={false}
          />
      );
  };



//   filterStock: ItemPredicate<IStock> = (query, stock, _index, exactMatch) => {
//     const normalizedTitle = stock.Symbol.toLowerCase();
//     const normalizedQuery = query.toLowerCase();

//     if (exactMatch) {
//         return (normalizedTitle === normalizedQuery)
//     }

//     else {
//         return `${stock.SymbolID}. ${normalizedTitle} ${stock.CompanyName}`.indexOf(normalizedQuery) >= 0;
//     }
//   };

//  highlightText(text: string, query: string) {
//     let lastIndex = 0;
//     const words = query
//         .split(/\s+/)
//         .filter(word => word.length > 0)
//         .map(this.escapeRegExpChars);
//     if (words.length === 0) {
//         return [text];
//     }
//     const regexp = new RegExp(words.join("|"), "gi");
//     const tokens: React.ReactNode[] = [];
//     while (true) {
//         const match = regexp.exec(text);
//         if (!match) {
//             break;
//         }
//         const length = match[0].length;
//         const before = text.slice(lastIndex, regexp.lastIndex - length);
//         if (before.length > 0) {
//             tokens.push(before);
//         }
//         lastIndex = regexp.lastIndex;
//         tokens.push(<strong key={lastIndex}>{match[0]}</strong>);
//     }
//     const rest = text.slice(lastIndex);
//     if (rest.length > 0) {
//         tokens.push(rest);
//     }
//     return tokens;
//   }

  handleClickOpen= () => {
    this.setState({ isOpen: true });
  }
  handleClickClose= () => {
    this.setState({ isOpen: false }); 
  }

//   areStocksEqual(stockA: IStock, stockB: IStock) {
//     // Compare only the titles (ignoring case) just for simplicity.
//     return stockA.Symbol.toLowerCase() === stockB.Symbol.toLowerCase();
// }

//  escapeRegExpChars(text: string) {
//     return text.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
//   }

handleStockSelect = (stock: IStock) => {
    console.log("!!!!!!!!!!");
    if (!this.isStockSelected(stock)) {
      this.selectStockItem(stock);
    } else {
      this.deselectStock(this.getSelectedStockIndex(stock));
    }
  };

  isStockSelected(stockItem) {

    return this.getSelectedStockIndex(stockItem) !== -1;
  }

  selectStockItem(stockItem) {
    console.log("@@@@@@@@@@");
    this.selectStock([stockItem]);
  }

  selectStock(stockToSelect) {
    console.log("fffffffff");
    console.log(stockToSelect);
    console.log(this.state.selectedStocks);
    const { createdItems, selectedStocks, items } = this.state;

    let nextCreatedItems = createdItems.slice();
    let nextStock = selectedStocks.slice();
    let nextItems = items.slice();

    stockToSelect.forEach(stockItem => {
        const results = this.maybeAddCreatedStockToArrays(nextItems, nextCreatedItems, stockItem);
        nextItems = results.items;
        nextCreatedItems = results.createdItems;
        // Avoid re-creating an item that is already selected (the "Create
        // Item" option will be shown even if it matches an already selected
        // item).
        nextStock = !this.arrayContainsStock(nextStock, stockItem) ? [...nextStock, stockItem] : nextStock;
    });

    this.setState({
        createdItems: nextCreatedItems,
        selectedStocks: nextStock,
        items: nextItems,
    });

    
}


maybeAddCreatedStockToArrays(
  items: IStock[],
  createdItems: IStock[],
  stock: IStock,
): { createdItems: IStock[]; items: IStock[] } {
  const isNewlyCreatedItem = !this.arrayContainsStock(items, stock);
  return {
      createdItems: isNewlyCreatedItem ? this.addStockToArray(createdItems, stock) : createdItems,
      // Add a created stock to `items` so that the stock can be deselected.
      items: isNewlyCreatedItem ? this.addStockToArray(items, stock) : items,
  };
}

arrayContainsStock(stock: IStock[], stockToFind: IStock): boolean {
  return stock.some((stock: IStock) => stock.title === stockToFind.title);
}

addStockToArray(stocks: IStock[], stockToAdd: IStock) {
  return [...stocks, stockToAdd];
}

  getSelectedStockIndex(stockItem : IStock) {
    return this.state.stock.indexOf(stockItem);
}

  renderTag = (s: IStock) => s.Symbol;
 

  getTagProps = (_value, index) => ({
    minimal: true,
  });
  
  handleTagRemove = (_tag, index) => {
    this.deselectStock(index);
  };

  deselectStock = (index) => {
    const { stock } = this.state;
    const stockItem = stock[index];
  };


     render() {  
      var rowIndex;
      var watchlist = null;
      const cellRenderer = (rowIndex) => {
        return <Cell></Cell>
      };

      let multiSelect;
      if(this.state.isOpen == true){
        multiSelect = 
        <div id = "addwatchlist">
        <Button icon = "minus" id = "icon" onClick = {this.handleClickClose}></Button>
        <StockMultiSelect
            items={this.state.STOCKS}
            itemRenderer={this.renderStock.bind(this)}
            onItemSelect={this.handleStockSelect()}
            noResults={<MenuItem disabled={true} text="Search stocks" />}
            tagRenderer={this.renderTag}
            tagInputProps={{ tagProps: this.getTagProps, onRemove: this.handleTagRemove }}
            selectedItems={this.state.selectedStocks}
            // itemsEqual ={this.areStocksEqual}
        />

      </div>
      }else{
        multiSelect = <Button icon = "insert" id = "addicon" onClick = {this.handleClickOpen}></Button>

        }

      // const clearButton =
      
      // (( (this.state.stock.length !== undefined) || (this.state.stock.length !== "undefined") ) && (this.state.stock.length  > 0 ) ) ? <Button icon="cross" minimal={true} onClick={this.handleClear} /> : undefined;
     

        return (
    <div>
        <div>
          <H2 className = "headings" style ={{fontSize: 15, color: Colors.WHITE}}> {"Market Dssata"} </H2>
           
          <div style={{display: "inline-block"}}>
          {multiSelect}
          </div>

        </div>
        <div className = "tables">
          <Table numRows={5} className={Classes.DARK}>
            <Column name="Symbol" cellRenderer={cellRenderer}/>
            <Column className = "columns" name = "Market Price" />
            <Column className = "columns" name = "Change"  />
            </Table>
        </div>
     </div>

        );
      }
     }
    