import React, { component } from 'react';
import { Cell, Column, Table, ColumnHeaderCell, RowHeaderCell } from "@blueprintjs/table";
import axios from 'axios';

import {
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames,
  button,
  Colors,
  MenuItem
} from "@blueprintjs/core";

import './datatables-styles-min.css'
import { Color } from 'highcharts';
import { Select } from "@blueprintjs/select";

// import * as $ from 'jquery';
// import DataTable from 'datatables.net'
const $ = require('jquery');
$.DataTable = require('datatables.net');


var dt;

export default class Strategies extends React.Component {
  constructor() {
    super();
    this.state = {
      allStrategyOrder: "",
      cellsData: []
    }
  }

  componentWillMount() {

    this.getStrategyOrderData();
    $('.data-table-wrapper')
      .find('table')
      .DataTable()
      .destroy(true);


    // $('#datatable_filter input').css("outline", "1px black solid");
  }

  componentDidMount() {
    dt = $(this.refs.main).DataTable({
      "searching": false,
      "bInfo" : false,
      'scrollY': 300,
      'scrollX': 100,
      'scrollCollapse': true,
      'lengthChange': false,
      'responsive': true,
      'paging': false,
      "language": {
        "emptyTable": "Start trading"
      },
      'columns': [
        {
          title: 'ID',
        },{
          title: 'Strategy',
        },{
          title: 'Stock Symbol',
        }, {
          title: 'Invested Amount',
        }, {
          title: 'Status',
        }, {
          title: 'Net',
        }, {
          title: 'Profit Lost',
        },{
          title: 'Actions'
        }

      ],
      "columnDefs": [ {
        "targets": -1,
        "data": null,
        "defaultContent": "<button>Click!</button>"
      }]
    });

    // setInterval(function() {
    //   dt.rows().invalidate().draw(); 
    // }, 1000 );

    $('#datatable tbody').on('click', 'button', 'tr', function () {
      let rowData = dt.row(this).data();
      console.log(rowData);
      if (rowData) {
        // router.navigate(['/booking-info', rowData[1], rowData[2]]);
      }
      
      
    });
  }


  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.state.cellsData != nextState.cellsData) {
  //     return false;
  //   }
  //   return true;
  // }

  // buttonsRenderer = () => {
  //   return (
  //     <Cell>
  //       <Button small = {true} icon = "play" id = "strategybutton1" />
  //       <Button small = {true} icon = "pause" id = "strategybutton2"/>
  //     </Cell>
  //   );
  // };


  // strategyRenderer = (rowIndex, columnIndex) =>{
  //   console.log("&&&&&&&&&&&&&&&&&&&&");
  //   console.log(this.state.allStrategyOrder);
  //   return(
  //     <Cell>
  //       {this.strategyData(rowIndex, columnIndex)}
  //     </Cell>
  //   )
  // }

  getStrategyOrderData() {
    axios.get("http://localhost:8081/StrategyOrder").then(res => {
      console.log("$$$$$$$$$$$$$$$$$$");
      //console.log(res.data);
      var list = [];

      for (var i = 0; i < res.data.length; i++) {
        console.log(res.data[i]);
        // list.push(Object.values(res.data[i]));

        var Id = res.data[i].Id;
        var Status = res.data[i].Status;

        var Invest = parseFloat(res.data[i].Invest);
        var InvestFormatted = "$" + (Invest).toFixed(2);

        var Net = parseFloat(res.data[i].Net);
        var NetFormatted = "$" + (Net).toFixed(2);

        var ProfitLost = parseFloat(res.data[i].ProfitLost);
        var ProfitLostFormatted = "$" + (ProfitLost).toFixed(2);

        var Stock = res.data[i].Stock;
        var Strategy = res.data[i].Strategy;
        console.log(dt);
        dt.row.add([Id, Strategy, Stock, InvestFormatted, Status, NetFormatted, ProfitLostFormatted]).draw();
      }

      this.render();

      this.setState({
        allStrategyOrder: res.data,
      })

      // () => {this.strategyRenderer();
      //   console.log("hkdjkdjklw");
      //   console.log(this.state.allStrategyOrder);
      //   }
      // )

    })
  }

  // render() {
  //   return (
  //     <Cell>
  //       <Button small = {true} icon = "play" id = "strategybutton1" intent = {Intent.SUCCESS}/>
  //       <Button small = {true} icon = "pause" id = "strategybutton2" intent = {Intent.WARNING}/>
  //     </Cell>
  //   );
  // };
  render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.

        return (
        <div>
        {/* <Cell>
          <Button small = {true} icon = "play" id = "strategybutton1" intent = {Intent.SUCCESS}/>
          <Button small = {true} icon = "pause" id = "strategybutton2" intent = {Intent.WARNING}/>
        </Cell>  */}
        
            {/* <div>
            <H2 className="headings" style = {{fontSize:15, color: Colors.WHITE, marginTop: 20}} columnWidth = "auto">Strategies</H2>
            </div>
            <div className= "rowComponents">
              <Table numRows={10} defaultRowHeight= {30} defaultColumnWidth= {140} className={Classes.DARK} enableColumnResizing={false}>
                <Column name= "Strategy Name"/>
                <Column name = "Symbol"/>
                <Column name = "Money Invested" />
                <Column name = "Status" />
                <Column name = "PnL" />
                <Column name = "Net" />
                <Column name = "Actions" cellRenderer={this.buttonsRenderer}/>
              </Table>
            </div>
         */}

      <div>
        <H2 className="headings" style={{ fontSize: 15, color: Colors.WHITE, marginTop: 30 }}>Strategies</H2>

        <table ref="main" />
      </div>
      </div>
        
        )};


  // reloadTableData(names) {
  //   const table = $('.data-table-wrapper')
  //     .find('table')
  //     .DataTable();
  //   table.clear();
  //   table.rows.add(names);
  //   table.draw();
  // }

  // updateTable(names) {
  //   const table = $('.data-table-wrapper')
  //     .find('table')
  //     .DataTable();
  //   let dataChanged = false;
  //   table.rows().every(function () {
  //     const oldNameData = this.data();
  //     const newNameData = names.find((nameData) => {
  //       return nameData.name === oldNameData.name;
  //     });
  //     if (oldNameData.nickname !== newNameData.nickname) {
  //       dataChanged = true;
  //       this.data(newNameData);
  //     }
  //     return true; // RCA esLint configuration wants us to 
  //     // return something
  //   });

  //   if (dataChanged) {
  //     table.draw();
  //   }
  // }

  // strategyData = (rowIndex, columnIndex) => {
  //   return this.state.allStrategyOrder[rowIndex][columnIndex];
  // };
  // render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.
  //   const table2 = 
  //     <div className = "tables" >
  //             <Table numRows={10} defaultRowHeight= {30} defaultColumnWidth= {140} className={Classes.DARK} enableColumnResizing={false}>
  //               <Column id ="strategyCol" name= "Strategy Name" cellRenderer={this.strategyRenderer}/>
  //               <Column id ="symbolCol" name = "Symbol" cellRenderer={this.strategyRenderer}/>
  //               <Column id ="moneyinvestedCol" name = "Money Invested" cellRenderer={this.strategyRenderer}/>
  //               <Column id ="statusCol" name = "Status" cellRenderer={this.strategyRenderer}/>
  //               <Column id ="pnlCol" name = "PnL" cellRenderer={this.strategyRenderer}/>
  //               <Column id ="netCol" name = "Net" cellRenderer={this.strategyRenderer}/>
  //               <Column id ="actionsCol" name = "Actions" cellRenderer={this.buttonsRenderer}/>
  //             </Table>
  //           </div>

  //       return (
  //         <div>
  //           <div>
  //           <H2 className="headings" style = {{fontSize:15, color: Colors.WHITE, marginTop: 30}}>Strategies</H2>
  //           </div>
  //           {table2}
  //         </div>
  //       )}
        }