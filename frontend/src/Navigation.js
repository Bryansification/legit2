import React, {Component} from 'react';
import { Colors } from "@blueprintjs/core";
import { Select } from "@blueprintjs/select";
import axios from 'axios';

import
{
  Alignment,
  Button,
  Classes,
  Navbar,
  NavbarDivider,
  NavbarGroup,
  NavbarHeading,
  
} from "@blueprintjs/core";

import './App.css';

export interface NavigationProps {
}
export default class Navigation extends React.PureComponent<NavigationProps>{
  constructor(props) {
    super(props);
    this.state = { 
     checking:false
  };
    this.getStrategyOrderData = this.getStrategyOrderData.bind(this);
}

  getStrategyOrderData() {
    axios.get("http://localhost:8081/StrategyOrder").then(res => {
      //console.log(res);
      //console.log(res.data);
      var sum = 0;
      var list = [];
      for (var i = 0; i < res.data.length; i++) {
        console.log(res.data[i]);
        // list.push(Object.values(res.data[i]));

        var Invest = parseFloat(res.data[i].Invest);
        var InvestFormatted = "$" + (Invest).toFixed(2);

        var Nett = parseFloat(res.data[i].Nett);
        var NettFormatted = "$" + (Nett).toFixed(2);

        var ProfitLost = parseFloat(res.data[i].ProfitLost);
        var ProfitLostFormatted = "$" + (ProfitLost).toFixed(2);
        sum += ProfitLost;

        var Stock = res.data[i].Stock;
        var Strategy = res.data[i].Strategy;
        //console.log(dt);
        // dt.row.add([Strategy, Stock, InvestFormatted, NettFormatted, ProfitLostFormatted]).draw();
      }
      setInterval( this.getStrategyData , 1000 );

      this.render();


      this.setState({
        allStrategyOrder: res.data,
      })

      // () => {this.strategyRenderer();
      //   console.log("hkdjkdjklw");
      //   console.log(this.state.allStrategyOrder);
      //   }
      // )

    })
  }

  render(){

    return(
      <Navbar className={Classes.DARK} style={{height: 80, backgroundColor: "#264757"}}>
        <NavbarGroup align={Alignment.LEFT} style={{marginTop: 10}}>
          <NavbarHeading>
          <div className = "projectlogo"> 
          <img id = "projectlogo" src= {require("./projectlogo1.png")} />
          </div>
          </NavbarHeading>
        </NavbarGroup>
        <NavbarGroup align={Alignment.RIGHT}  style={{marginTop: 20}}>
          <Button className={Classes.MINIMAL} style={{marginRight: 20, textAlign:"center"}}> Wallet Limit<br/><br/> S$10,000,000</Button>
          <NavbarDivider />
          <Button className={Classes.MINIMAL} style={{marginLeft: 20, textAlign:"center"}}> Net Worth <br/><br/>(Net Worth)</Button>
          <NavbarDivider />
          <Button className={Classes.MINIMAL} style={{marginLeft: 20, marginRight:30, textAlign:"center"}}> PnL <br/><br/>{this.getStrategyOrderData.sum}</Button>
        </NavbarGroup>
      </Navbar>

  )}

}
