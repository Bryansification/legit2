package citi.com.project;

public interface CONSTANT {
	public static final int _SHORT = 20;
	public static final int _LONG = 60;
	public static final double _MONEYINTHEBAG = 10_000_000;
}
