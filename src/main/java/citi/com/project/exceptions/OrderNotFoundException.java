package citi.com.project.exceptions;

public class OrderNotFoundException extends Exception {

	public OrderNotFoundException(int id) {
		super("Order Id Not Found : " + id);
	}
}
