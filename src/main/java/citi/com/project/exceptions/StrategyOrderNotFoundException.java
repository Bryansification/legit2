package citi.com.project.exceptions;

public class StrategyOrderNotFoundException extends Exception{
	public StrategyOrderNotFoundException(int id ) {
		super("Strategy Order Id Not Found : " + id);
	}
	
}
