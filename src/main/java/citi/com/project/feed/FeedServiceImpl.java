package citi.com.project.feed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.http.HTTPException;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import static citi.com.project.CONSTANT._SHORT;
import static citi.com.project.CONSTANT._LONG;

import citi.com.project.exceptions.PageNotFoundException;
import citi.com.project.strategy.implementation.Response;

@Service
public class FeedServiceImpl implements FeedService{

	
	String symbolListUrl = "http://feed2.conygre.com/API/StockFeed/GetSymbolList";
	String symbolDetails = "http://feed2.conygre.com/API/StockFeed/GetSymbolDetails/";
	String stockPriceUrl1 = "http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/";
	String stockPriceUrl2 = "?HowManyValues=";
	
	@Override
	public Map<String, Double> getWatchList(List<String> reqWatchList) throws PageNotFoundException {
		Map<String, Double> watchList = new HashMap<String, Double>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
			for(String sym : reqWatchList){
				String shortAvg = stockPriceUrl1 + sym + stockPriceUrl2 + 1;
				ResponseEntity<List<Response>> response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
				watchList.put(sym, response.getBody().get(0).getPrice());
			}
		}catch(Exception e) {
			throw new PageNotFoundException(0);
		}
		return watchList;
	}
	@Override
	public List<Double> getSMAAverage(String stockSymbol) throws PageNotFoundException {
		List<Double> smaAvg = new ArrayList<Double>();
		smaAvg.add(callAvgFeed(stockSymbol, true));
		smaAvg.add(callAvgFeed(stockSymbol, false));
		return smaAvg;
	}
	
	private double callAvgFeed(String stockSymbol, boolean isShort) throws PageNotFoundException {
		int numberOfStocks = (isShort) ? _SHORT : _LONG;
		RestTemplate restTemplate = new RestTemplate();
		String shortAvg = stockPriceUrl1 + stockSymbol + stockPriceUrl2 + numberOfStocks;
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		ResponseEntity<List<Response>> response = null;
		try {
			response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
		}catch(Exception e) {
			throw new PageNotFoundException(0);
		}
		double total = 0;
		for (Response resp: response.getBody()) {
			total += resp.getPrice();
		}
		
		return (total/numberOfStocks);
	}
	
	@Override
	public double getMarketPrice(String stockCode) throws PageNotFoundException {
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		String shortAvg = stockPriceUrl1 + stockCode + stockPriceUrl2 + 1;
		ResponseEntity<List<Response>> response = null;
		try {
			response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
		}catch(Exception e) {
			throw new PageNotFoundException(0);
		}
		return response.getBody().get(0).getPrice();
	}
	
	@Override
	public List<Double> getMarketPriceMultiPast(String stockCode, int number) throws PageNotFoundException {
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		String shortAvg = stockPriceUrl1 + stockCode + stockPriceUrl2 + number;
		ResponseEntity<List<Response>> response = null;
		try {
			response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
		}catch(Exception e) {
			throw new PageNotFoundException(0);
		}
		List<Double> resultList = new ArrayList<Double>();
		for(Response res : response.getBody()) {
			resultList.add(res.getPrice());
		}
		return resultList;
	}
	
	@Override
	public List<Map<String, String>> getSymbolList() throws PageNotFoundException {
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		ResponseEntity<List<Response>> response = null;
		try {
			response = restTemplate.exchange(symbolListUrl, HttpMethod.GET, null, responseType);
		}catch(Exception e) {
			throw new PageNotFoundException(0);
		}
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		for(Response res : response.getBody()) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("CompanyName", res.getCompanyName());
			map.put("Symbol", res.getSymbol());
			resultList.add(map);
		}
		return resultList;
	}
	@Override
	public List<Double> getBBSD(String stockSymbol) throws PageNotFoundException {
		int numberOfStocks =  _SHORT;
		List<Double> returnList = new ArrayList<Double>();
		RestTemplate restTemplate = new RestTemplate();
		String shortAvg = stockPriceUrl1 + stockSymbol + stockPriceUrl2 + numberOfStocks;
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		ResponseEntity<List<Response>> response = null;
		try {
			response = restTemplate.exchange(shortAvg, HttpMethod.GET, null, responseType);
		}catch(Exception e) {
			throw new PageNotFoundException(0);
		}
		List<Double> priceList = new ArrayList<Double>();
		for (Response resp: response.getBody()) {
			priceList.add(resp.getPrice());
		}
		double mean = priceList.stream().mapToDouble(Double::doubleValue).sum()/priceList.size();
		returnList.add(mean);
        double n = priceList.size();
        double dv = 0;
        for (double d : priceList) {
            double dm = d - mean;
            dv += dm * dm;
        }
        returnList.add(Math.sqrt(dv/n));
        return returnList;
	}

	public Map<String, Double> getOHLC(String stockCode, int periodInSecs) throws PageNotFoundException {
		RestTemplate restTemplate = new RestTemplate();
		String stockData = stockPriceUrl1 + stockCode + stockPriceUrl2 + periodInSecs;
		ParameterizedTypeReference<List<Response>> responseType = new ParameterizedTypeReference<List<Response>>() {};
		ResponseEntity<List<Response>> response = null;
		try {
			response = restTemplate.exchange(stockData, HttpMethod.GET, null, responseType);
		}catch(Exception e) {
			throw new PageNotFoundException(0);
		}
		ArrayList<Double> stockPrices = new ArrayList<>();
		for(Response r : response.getBody()) {
			stockPrices.add(r.getPrice());
		}
		
		Map<String, Double> ohlc = new HashMap<>();
		ohlc.put("open", stockPrices.get(0));
		ohlc.put("high", Collections.max(stockPrices));
		ohlc.put("low", Collections.min(stockPrices));
		ohlc.put("close", stockPrices.get(stockPrices.size()-1));
		
		return ohlc;
	}
}
