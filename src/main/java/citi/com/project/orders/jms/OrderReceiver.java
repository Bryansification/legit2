package citi.com.project.orders.jms;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import citi.com.project.orders.entity.BrokerOrders;
import citi.com.project.orders.entity.OrderStatus;

@Component
public class OrderReceiver {

	private Logger logger = LoggerFactory.getLogger(OrderReceiver.class);

	@Autowired
	JmsTemplate jmsTemplate;

	private static final String source = "OrderBroker";
	private static final String destination = "OrderBroker_Reply";

	Random random = new Random();

	@JmsListener(destination = source, containerFactory = "myFactory")
	public void receiveMessage(@Payload BrokerOrders order, @Header(JmsHeaders.CORRELATION_ID) String correlationId) {
		logger.info("Received order at Broker <" + order + ">");

		if (correlationId == null || correlationId.equals("")) {
			logger.info("ERROR 101 : No correlation ID detected.");
		} else {

			OrderStatus result;
			int responseNo = random.nextInt(100);

			if (responseNo < 33) // Set FILL percentage
				result = OrderStatus.FILLED;
			else if (responseNo < 66) // Set PARTIALLY FILL percentage
				result = OrderStatus.PARTIALLY_FILLED;
			else
				result = OrderStatus.REJECTED;

			int fillQuantity = order.getFillQuantity();

			if (order.isExits()) {

				result = OrderStatus.FILLED;
				fillQuantity = order.getReqQuantity() - order.getFillQuantity();

			} else {
				if (result == OrderStatus.FILLED) {
					fillQuantity = order.getReqQuantity();
				} else if (result == OrderStatus.PARTIALLY_FILLED) {
					fillQuantity += random.nextInt(order.getReqQuantity() - order.getFillQuantity());
					if(fillQuantity == order.getReqQuantity()) {
						result = OrderStatus.FILLED;
					}
				} else {
					fillQuantity = order.getFillQuantity();
				}
			}

	//		BrokerOrders orderReply = new BrokerOrders(order.getOrderId(), order.isBuy(), order.isExits(), order.getRetryCount(),
	//				order.getPrice(), order.getReqQuantity(), fillQuantity, order.getOrderDate(), result,
	//				order.getStockCode());

			// HAPPY MODE
			BrokerOrders orderReply = new BrokerOrders(order.getOrderId(), order.isBuy(), order.isExits(),
					order.getRetryCount(), order.getPrice(), order.getReqQuantity(), order.getReqQuantity(),
					order.getOrderDate(), OrderStatus.FILLED, order.getStockCode());

			try {
				//Thread.sleep(random.nextInt(10_000));
				jmsTemplate.convertAndSend(destination, orderReply, m -> {
					m.setJMSCorrelationID(correlationId);
					return m;
				});
				logger.info("OrderReply sent at Broker <" + orderReply + ">");
			} catch (Exception ex) {
				logger.info("An error has occurred while trying to send an order reply for up to 10 seconds.");
			}
		}
	}

}
