package citi.com.project.strategy.entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import citi.com.project.orders.entity.Orders;
import citi.com.project.stocks.entity.Stock;
import lombok.Data;


@Entity
@Data
public class StrategyOrders {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int stratOrderId;
	private double requestAmnt = 0.0;
	
	private int stockWallet = 0;
	private double moneywallet = 0.0 ;
	
	@Transient
	private String requestStockCode;
	@Transient
	private String requestStrategyCode;
	
	

	private StrategyOrderStatus status = StrategyOrderStatus.LIVE;
	private int smaIndicator = 99 ;	//Default 99
	
	private double pbOpen;	//Null
	private double pbClose;	//Null
	private int pbPeriod; //Null
	private int pbTrend; //Null
	
	
	@ManyToOne
	@JoinColumn(name="strategy_id", nullable=false)
	private Strategy strategy;
	
	@ManyToOne
	@JoinColumn(name="stock_id", nullable=false)
	private Stock stock;
	
	@OneToMany
	@JoinColumn(name = "strat_order_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Collection<Orders> orderHistory;
	
	public StrategyOrders(Stock stock, Strategy strategy, double limit) {
		this.stock = stock;
		this.strategy = strategy;
		this.requestAmnt = limit;
		this.moneywallet = limit;
	}
	
	public StrategyOrders() {}
}	

	
	

