package citi.com.project.stocks.repo;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.entity.StockFeedData;

@Repository
public interface StockFeedDataRepository extends CrudRepository<StockFeedData, Integer>{

	
	
	public Collection<StockFeedData> findAllByStock(Stock stock);
	public Collection<StockFeedData> findAllByPullDateBetween(Date startDate, Date endDate);
}
